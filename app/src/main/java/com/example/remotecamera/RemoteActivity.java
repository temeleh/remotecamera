package com.example.remotecamera;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.toolbox.NetworkImageView;
import com.example.remotecamera.util.VolleyNetworking;

import java.nio.charset.StandardCharsets;

public class RemoteActivity extends AppCompatActivity {

    private NetworkImageView imageView;
    private VolleyNetworking volley;
    private ToggleButton facingButton;

    private String passphrase;
    private String urlBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);

        passphrase = getIntent().getStringExtra(getString(R.string.passphrase));

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        urlBase = prefs.getString(getString(R.string.url_base), "");

        imageView = findViewById(R.id.imageView);
        imageView.setErrorImageResId(android.R.drawable.ic_dialog_alert);

        facingButton = findViewById(R.id.facingButton);
        facingButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                loadImage();
            }
        });

        volley = VolleyNetworking.getInstance(this);

        String cameraSide = prefs.getString(getString(R.string.cameraside), "");
        if (cameraSide != null && cameraSide.equals("Front")) {
            facingButton.setChecked(true); // loads image onCheckedChanged
        } else {
            loadImage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_remote, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void refresh(MenuItem menu) {
        loadImage();
    }

    private void loadImage() {
        if (urlBase.isEmpty()) {
            return;
        }

        String token = Base64.encodeToString(passphrase.getBytes(StandardCharsets.UTF_8), Base64.URL_SAFE | Base64.NO_PADDING | Base64.NO_WRAP);
        String facing = facingButton.isChecked() ? "front" : "back";

        String url = urlBase + "?token=" + token + "&facing=" + facing;

        imageView.setImageUrl("", volley.getImageLoader());
        imageView.setImageUrl(url, volley.getImageLoader());
    }
}
