package com.example.remotecamera.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.remotecamera.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilenameFilter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public class UploadLatestImageWorker extends Worker {

    public UploadLatestImageWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String passphrase = getInputData().getString(this.getApplicationContext().getString(R.string.passphrase));
        String urlBase = getInputData().getString(this.getApplicationContext().getString(R.string.url_base));

        // output directories
        File frontDir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator
                + "RemoteCamera" + File.separator + passphrase + File.separator + "Front");
        File backDir = new File(Environment.getExternalStorageDirectory().getPath() + File.separator
                + "RemoteCamera" + File.separator + passphrase + File.separator + "Back");

        if ((!frontDir.mkdirs() && !frontDir.isDirectory()) || (!backDir.mkdirs() && !backDir.isDirectory())) {
            return Result.failure(); // Could not make directories
        }

        File latestFront = getLatestImage(frontDir);
        File latestBack = getLatestImage(backDir);

        if (latestFront == null && latestBack == null) {
            return Result.retry(); // No images found
        }

        uploadImage(latestFront, passphrase, urlBase, true);
        uploadImage(latestBack, passphrase, urlBase, false);

        return Result.success();
    }

    private void uploadImage(File image, String passphrase, String url_base, boolean isFrontFacing) {
        if (image == null)
            return;

        byte[] bytes = readWithTimestamp(image);
        String token = Base64.encodeToString(passphrase.getBytes(StandardCharsets.UTF_8), Base64.URL_SAFE | Base64.NO_PADDING | Base64.NO_WRAP);
        String facing = isFrontFacing ? "front" : "back";

        String url = url_base + "?token=" + token + "&facing=" + facing;

        JPEGPutRequest request = new JPEGPutRequest(bytes, url);
        VolleyNetworking.getInstance(this.getApplicationContext()).addToRequestQueue(request);
    }

    /**
     * @param dir searched directory
     * @return latest .jpg file in dir, or null if no .jpg files were found
     */
    private File getLatestImage(File dir) {
        File[] images = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".jpg");
            }
        });

        Arrays.sort(images, new Comparator<File>() {
            @Override
            public int compare(File file1, File file2) {
                return Long.compare(file2.lastModified(), file1.lastModified());
            }
        });

        if (images.length == 0) {
            return null;
        }
        return images[0];
    }

    // https://stackoverflow.com/q/33970715
    private byte[] readWithTimestamp(File file) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String dateTime = sdf.format(new Date(file.lastModified()));

        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        Bitmap dest = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(bitmap, 0f, 0f, null);

        Paint paint = new Paint();
        paint.setTextSize(35);
        float height = bitmap.getHeight() - paint.measureText("yY");

        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawText(dateTime, 20f, height, paint);

        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawText(dateTime, 20f, height, paint);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        dest.compress(Bitmap.CompressFormat.JPEG, 90, bos);

        return bos.toByteArray();
    }

    @Override
    public void onStopped() {
        Log.d("UploadLatestImageWorker", "Stopped!");
    }

    private static class JPEGPutRequest extends StringRequest {
        private byte[] imageBytes;

        JPEGPutRequest(byte[] imageBytes, String url) {
            super(Method.PUT, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("OnResponse", "response: " + response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("OnErrorResponse", "error: " + error.toString());
                        }
                    });

            this.imageBytes = imageBytes;
        }

        @Override
        public byte[] getBody() {
            return imageBytes;
        }

        @Override
        public String getBodyContentType() {
            return "image/jpeg";
        }
    }
}
