/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.remotecamera.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.*;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.*;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import com.example.remotecamera.CameraActivity;
import com.example.remotecamera.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static com.example.remotecamera.util.CameraService.State.*;

public class CameraService extends Service {


    private static final int IMAGES_BEFORE_CAPTURE = 30;

    private static final String TAG = "CameraService";


    /**
     * Rotation of the screen
     */
    private int screenRotation = -1;

    /**
     * Passphrase of this RemoteCamera instance
     */
    private String passphrase;


    private static class Camera {

        /**
         * ID of the current {@link CameraDevice}.
         */
        String cameraId;

        /**
         * Facing of the camera
         */
        int cameraFacing;

        /**
         * Orientation of the camera sensor
         */
        int sensorOrientation;

        /**
         * Main resolution for this camera
         */
        Size selectedRes;

        /**
         * Highest resolution available for this camera
         */
        Size highestRes;



        Camera(String cameraId, int cameraFacing, int sensorOrientation, Size selectedRes, Size highestRes) {
            this.cameraId = cameraId;
            this.cameraFacing = cameraFacing;
            this.sensorOrientation = sensorOrientation;

            this.selectedRes = selectedRes;
            this.highestRes = highestRes;
        }
    }

    /**
     * All cameras
     */
    private List<Camera> cameras;

    /**
     * Currently active camera index
     */
    private Camera activeCamera;

    /**
     * If true uses highest resolution for image capture
     */
    private boolean useHighestRes;

    /**
     * Camera manager
     */
    private CameraManager manager;


    // Values from settings
    private Constant.Resolution frontRes;
    private Constant.Resolution backRes;
    private int frontQuality;
    private int backQuality;


    public CameraService() {
        super();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        if (intent == null) {
            screenRotation = prefs.getInt(getString(R.string.screenrotation), -1);
            passphrase = prefs.getString(getString(R.string.passphrase), "passphrase");
        } else {
            screenRotation = intent.getIntExtra(getString(R.string.screenrotation), -1);
            passphrase = intent.getStringExtra(getString(R.string.passphrase));
        }

        int delay = Integer.parseInt(Objects.requireNonNull(prefs.getString(getString(R.string.delay), "20000")));
        int count = Integer.parseInt(Objects.requireNonNull(prefs.getString(getString(R.string.count), "1")));
        boolean dailyHQ = prefs.getBoolean(getString(R.string.dailyhq), false);

        Constant.Camera cameraSide = Constant.Camera.valueOf(Objects.requireNonNull(prefs.getString(getString(R.string.cameraside), Constant.Camera.BACK.name())).toUpperCase());
        frontRes = Constant.Resolution.valueOf(prefs.getString(getString(R.string.frontres), Constant.Resolution.HD.name()));
        backRes = Constant.Resolution.valueOf(prefs.getString(getString(R.string.backres), Constant.Resolution.HD.name()));
        frontQuality = Integer.parseInt(Objects.requireNonNull(prefs.getString(getString(R.string.frontquality), "100")));
        backQuality = Integer.parseInt(Objects.requireNonNull(prefs.getString(getString(R.string.backquality), "100")));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }

        Intent notificationIntent = new Intent(this, CameraActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, Constant.CHANNEL_ID)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(getResources().getString(R.string.capturing))
                .setSmallIcon(R.drawable.notification_icon)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        startBackgroundThread();

        manager = (CameraManager) this.getApplicationContext().getSystemService(Context.CAMERA_SERVICE);
        setUpCameraOutputs(cameraSide, manager);

        if (screenRotation == -1 || passphrase.isEmpty() || cameras.size() == 0) {
            stopSelf();
            return START_NOT_STICKY;
        }

        int hqCount = 0;
        int hqDelay = 43200000; //12h
// TODO: 18/11/2019 replace with alarmmanager
        for (int i = 0; i < count; i++) {
            if (dailyHQ && i * delay >= hqDelay * hqCount) {
                hqCount++;

                backgroundHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        useHighestRes = true;
                        openCamera(cameras.get(0));
                    }
                }, delay * i);
            } else {
                backgroundHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        useHighestRes = false;
                        openCamera(cameras.get(0));
                    }
                }, delay * i);
            }
        }

        backgroundHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast("Finished!");
                stopSelf();
            }
        }, delay * (count - 1) + 10000);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        close();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        NotificationChannel serviceChannel = new NotificationChannel(
                Constant.CHANNEL_ID,
                Constant.CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
        );

        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(serviceChannel);
    }





    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    /**
     * Retrieves the JPEG orientation from the specified screen rotation.
     *
     * @param rotation The screen rotation.
     * @return The JPEG orientation (one of 0, 90, 270, and 180)
     */
    private int getOrientation(int rotation) {
        if (activeCamera.cameraFacing == CameraCharacteristics.LENS_FACING_FRONT && rotation % 2 != 0) {
            rotation = (rotation + 2) % 4;
        }

        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (ORIENTATIONS.get(rotation) + activeCamera.sensorOrientation + 270) % 360;
    }


    enum State {
        STATE_PREVIEW, // Showing camera preview.
        STATE_WAITING_LOCK, // Waiting for the focus to be locked.
        STATE_WAITING_PRECAPTURE, // Waiting for the exposure to be precapture state.
        STATE_WAITING_NON_PRECAPTURE, // Waiting for the exposure state to be something other than precapture.
        STATE_PICTURE_TAKEN // Picture was taken.
    }



    /**
     * A {@link CameraCaptureSession }.
     */
    private CameraCaptureSession captureSession;

    /**
     * A reference to the opened {@link CameraDevice}.
     */
    private CameraDevice cameraDevice;




    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
     */
    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            cameraOpenCloseLock.release();
            CameraService.this.cameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraOpenCloseLock.release();
            cameraDevice.close();
            CameraService.this.cameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            onDisconnected(cameraDevice);
            Log.e(TAG, "CameraDevice StateCallBack error: " + error);
        }

    };

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread backgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler backgroundHandler;

    /**
     * An {@link ImageReader} that handles still image capture.
     */
    private ImageReader imageReader;

    /**
     * This a callback object for the {@link ImageReader}. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private final ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(final ImageReader reader) {
            backgroundHandler.post(new Runnable() {
                @Override
                public void run() {
                    saveImage(reader.acquireLatestImage());
                }
            });
        }
    };


    private SurfaceTexture previewTexture = new SurfaceTexture(1);
    private Surface previewSurface = new Surface(previewTexture);

    /**
     * {@link CaptureRequest.Builder} for the camera preview
     */
    private CaptureRequest.Builder previewRequestBuilder;

    /**
     * {@link CaptureRequest} generated by {@link #previewRequestBuilder}
     */
    private CaptureRequest previewRequest;

    /**
     * The current state of camera state for taking pictures.
     *
     * @see #captureCallback
     */
    private State state = STATE_PREVIEW;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore cameraOpenCloseLock = new Semaphore(1);


    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {

        private int previewImageCount = 0;

        private void process(CaptureResult result) {
            switch (state) {
                case STATE_PREVIEW: {
                    if (result instanceof TotalCaptureResult) {
                        previewImageCount++;
                    }
                    if (previewImageCount == IMAGES_BEFORE_CAPTURE) {
                        previewImageCount = 0;
                        startCapture();
                    }
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            state = STATE_PICTURE_TAKEN;
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        state = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        state = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }
    };


    /**
     * Sets up member variables related to camera.
     */
    private void setUpCameraOutputs(Constant.Camera cameraSide, CameraManager manager) {
        cameras = new ArrayList<>(2);

        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

                Integer currentFacing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (currentFacing == null || (
                        (cameraSide == Constant.Camera.FRONT && currentFacing != CameraCharacteristics.LENS_FACING_FRONT) ||
                        (cameraSide == Constant.Camera.BACK && currentFacing != CameraCharacteristics.LENS_FACING_BACK))) {
                    continue;
                }

                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                List<Size> outputSizes = Arrays.asList(map.getOutputSizes(ImageFormat.JPEG));
                Collections.sort(outputSizes, new Comparator<Size>() {
                    @Override
                    public int compare(Size lhs, Size rhs) {
                        // We cast here to ensure the multiplications won't overflow
                        return Long.signum((long) lhs.getWidth() * lhs.getHeight() - (long) rhs.getWidth() * rhs.getHeight());
                    }
                });

                Constant.Resolution settingsRes = currentFacing == CameraCharacteristics.LENS_FACING_BACK ? backRes : frontRes;
                Size selectedSize = outputSizes.get(outputSizes.size() - 1);

                for (Size size : outputSizes) {
                    if ((long) size.getWidth() * size.getHeight() >= settingsRes.area) {
                        selectedSize = size;
                        break;
                    }
                }

                //noinspection ConstantConditions
                cameras.add(new Camera(
                        cameraId,
                        currentFacing,
                        characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION),
                        selectedSize,
                        outputSizes.get(outputSizes.size() - 1)
                ));

                if (cameraSide != Constant.Camera.BOTH)
                    return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the device this code runs.
            showToast("Camera2 API not supported on this device!!");
        }
    }


    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            if (imageReader == null || cameraDevice == null) {
                return;
            }

            // We set up a CaptureRequest.Builder with the output Surface.
            previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewRequestBuilder.addTarget(previewSurface);

            // Here, we create a CameraCaptureSession for camera preview.
            cameraDevice.createCaptureSession(Arrays.asList(previewSurface, imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (cameraDevice == null) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            captureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                                // Finally, we start displaying the camera preview.
                                previewRequest = previewRequestBuilder.build();
                                captureSession.setRepeatingRequest(previewRequest, captureCallback, backgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            showToast("Configure failed!!");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    private void showToast(final String text) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(CameraService.this.getApplicationContext(), text, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Opens the given camera
     *
     * Automatically takes a picture when opened and closes after picture is taken.
     */
    private void openCamera(Camera camera) {
        if (captureSession != null) {
            closeCamera();
            // if still camera open, return and don't open the camera
            if (captureSession != null) {
                showToast("Could not open camera!");
                return;
            }
        }
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            showToast("No camera permission");
            stopSelf();
            return;
        }

        activeCamera = camera;

        Size resolution;
        if (useHighestRes)
            resolution = activeCamera.highestRes;
        else
            resolution = activeCamera.selectedRes;

        imageReader = ImageReader.newInstance(resolution.getWidth(), resolution.getHeight(), ImageFormat.JPEG, /*maxImages*/2);
        imageReader.setOnImageAvailableListener(onImageAvailableListener, backgroundHandler);

        try {
            if (!cameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(activeCamera.cameraId, stateCallback, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            stopSelf();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            state = STATE_PREVIEW;

            cameraOpenCloseLock.acquire();
            if (captureSession != null) {
                captureSession.close();
                captureSession = null;
            }
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
            if (imageReader != null) {
                imageReader.close();
                imageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            cameraOpenCloseLock.release();
        }
        Log.d(TAG, "Closed camera!");
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        backgroundThread = new HandlerThread("CameraBackground");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        backgroundThread.quitSafely();
        try {
            backgroundThread.join();
            backgroundThread = null;
            backgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * Closes the camera and stops the background thread.
     */
    private void close() {
        closeCamera();
        stopBackgroundThread();
    }


    private void startCapture() {
        if (activeCamera.cameraFacing == CameraCharacteristics.LENS_FACING_BACK) {
            lockFocus();
        } else {
            captureStillPicture();
        }
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            previewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            state = STATE_WAITING_LOCK;
            captureSession.capture(previewRequestBuilder.build(), captureCallback,
                    backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #captureCallback} from {@link #lockFocus()}.
     */
    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            previewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            state = STATE_WAITING_PRECAPTURE;
            captureSession.capture(previewRequestBuilder.build(), captureCallback,
                    backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #captureCallback} from both {@link #lockFocus()}.
     */
    private void captureStillPicture() {
        try {
            if (cameraDevice == null) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(imageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

            // Orientation
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(screenRotation));

            CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    unlockFocus();

                    // closing camera after capture
                    closeCamera();
//                    stopBackgroundThread();

                    // activate second camera if capturing with both cameras
                    if (cameras.size() == 2 && activeCamera == cameras.get(0)) {
                        Log.d(TAG, "Activating second camera");
                        openCamera(cameras.get(1));
                    } else {
                        Log.d(TAG, "Image captured");
                    }
                }
            };

            captureSession.stopRepeating();
            captureSession.abortCaptures();
            captureSession.capture(captureBuilder.build(), captureCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            previewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);

            captureSession.capture(previewRequestBuilder.build(), captureCallback,
                    backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("SimpleDateFormat")
    private void saveImage(Image image) {
        // get bytes and compress
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);

        boolean frontFacing = activeCamera.cameraFacing == CameraCharacteristics.LENS_FACING_FRONT;

        int quality = 90;
        if (!useHighestRes) {
            quality = frontFacing ? frontQuality : backQuality;
        }

        Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, quality, bos);

        bytes = bos.toByteArray();

        String folder = "RemoteCamera" + File.separator + passphrase;
        if (useHighestRes) folder += "_HQ";

        if (frontFacing) folder += File.separator + "Front";
        else folder += File.separator + "Back";

        // get output directory
        File storageDir = new File(Environment.getExternalStorageDirectory().getPath()
                + File.separator + folder);

        if ((!storageDir.mkdirs() && !storageDir.isDirectory())) {
            return;
        }

        // save bytes to a file
        File file = new File(storageDir, new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss").format(new Date()) + ".jpg");

        FileOutputStream output = null;
        try {
            output = new FileOutputStream(file);
            output.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            image.close();
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
