package com.example.remotecamera.util;

public class Constant {
    public static final String UPLOAD_WORK = "uploadwork";

    static final String CHANNEL_ID = "CameraServiceChannel";
    static final String CHANNEL_NAME = "Camera Service Channel";

    public enum Camera {
        FRONT, BACK, BOTH
    }

    @SuppressWarnings("unused")
    public enum Resolution {
        LOW(0, 0), HD(1280, 720), FHD(1920, 1080), QHD(2560, 1440), UHD(3840, 2160), HIGH(-1, -1);

        int area;

        Resolution(int width, int height) {
            this.area = width * height; //no overflow with these values
        }
    }
}
