package com.example.remotecamera.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class VolleyNetworking {
    @SuppressLint("StaticFieldLeak")
    private static VolleyNetworking instance;

    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    @SuppressLint("StaticFieldLeak")
    private static Context ctx;

    private VolleyNetworking(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue,
                new ImageLoader.ImageCache() { // no cache
                    @Override
                    public Bitmap getBitmap(String url) {
                        return null;
                    }
                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {}
                });
    }

    public static synchronized VolleyNetworking getInstance(Context context) {
        if (instance == null) {
            instance = new VolleyNetworking(context);
        }
        return instance;
    }

    private RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }
}

