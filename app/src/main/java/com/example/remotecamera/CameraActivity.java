package com.example.remotecamera;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.work.*;
import com.example.remotecamera.util.CameraService;
import com.example.remotecamera.util.Constant;
import com.example.remotecamera.util.UploadLatestImageWorker;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class CameraActivity extends AppCompatActivity {

    private Button captureButton;

    private String passphrase;
    private String urlBase;

    private PeriodicWorkRequest uploadRequest;
    private State currentState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        captureButton = findViewById(R.id.captureButton);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateState();

                switch (currentState) {
                    case CAPTURE_UPLOAD:
                        confirmStop("Stop capturing and uploading?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopUploadWorker();
                                stopCaptureService();
                                updateState();
                            }
                        });
                        break;
                    case CAPTURE:
                        confirmStop("Stop capturing?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopCaptureService();
                                updateState();
                            }
                        });
                        break;
                    case UPLOAD:
                        confirmStop("Stop uploading?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopUploadWorker();
                                updateState();
                            }
                        });
                        break;
                    case DEFAULT:
                        confirmStop("Start capturing and uploading?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startCapture();
                                updateState();
                            }
                        });
                        break;
                }
            }
        });

        passphrase = getIntent().getStringExtra(getString(R.string.passphrase));

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        urlBase = prefs.getString(getString(R.string.url_base), "");

        updateState();
        requestPermissions();

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        Data inputData = new Data.Builder()
                .putString(getString(R.string.passphrase), passphrase)
                .putString(getString(R.string.url_base), urlBase)
                .build();

        uploadRequest = new PeriodicWorkRequest.Builder(UploadLatestImageWorker.class, 15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .setInputData(inputData)
                .setInitialDelay(5, TimeUnit.SECONDS)
                .build();
    }

    private void confirmStop(String message, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder a = new AlertDialog.Builder(this);
        a.setPositiveButton("Yes", clickListener);
        a.setMessage(message);
        a.setTitle("Are you sure?");
        a.setNegativeButton("No", null);
        a.setIcon(android.R.drawable.ic_dialog_alert);
        a.show();
    }

    private void startCapture() {
        Intent serviceIntent = new Intent(this, CameraService.class);

        int screenRotation = getWindowManager().getDefaultDisplay().getRotation();
        serviceIntent.putExtra(getString(R.string.screenrotation), screenRotation);
        serviceIntent.putExtra(getString(R.string.passphrase), passphrase);

        // Save latest rotation and passphrase
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(getString(R.string.screenrotation), screenRotation);
        editor.putString(getString(R.string.passphrase), passphrase);
        editor.apply();

        ContextCompat.startForegroundService(this, serviceIntent);

        if (!urlBase.isEmpty()) {
            // start uploading latest image
            WorkManager manager = WorkManager.getInstance(this);
            manager.enqueueUniquePeriodicWork(Constant.UPLOAD_WORK, ExistingPeriodicWorkPolicy.REPLACE, uploadRequest);
        }

        updateState();
    }

    private void stopCaptureService() {
        Intent intent = new Intent(this, CameraService.class);
        stopService(intent);
    }

    private boolean isCaptureProcessRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        //noinspection deprecation
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (CameraService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void stopUploadWorker() {
        WorkManager.getInstance(this).cancelAllWork();
    }

    private boolean isUploadWorkerRunning() {
        List<WorkInfo> works = null;
        try {
            works = WorkManager.getInstance(this).getWorkInfosForUniqueWork(Constant.UPLOAD_WORK).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if (works != null && works.size() > 0) {
            return !works.get(0).getState().isFinished();
        }
        return false;
    }

    private boolean isUploadAvailable() {
        // has network/wifi activated
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && !urlBase.isEmpty();
    }

    private void updateState() {
        if (isCaptureProcessRunning()) {
            if (isUploadWorkerRunning()) {
                currentState = State.CAPTURE_UPLOAD;
                captureButton.setText(getString(R.string.stop_capture_upload));
            } else {
                currentState = State.CAPTURE;
                captureButton.setText(getString(R.string.stop_capturing));
            }
            captureButton.setTextColor(0xff151515);
            captureButton.setBackgroundResource(R.color.colorPrimaryDark);

            // full screen and set black background
            getWindow().getDecorView().getRootView().setBackgroundResource(R.color.colorPrimaryDark);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        } else {
            if (isUploadWorkerRunning()) {
                currentState = State.UPLOAD;
                captureButton.setText(getString(R.string.stop_upload));
                captureButton.setTextColor(0xff9267c6);
            } else {
                currentState = State.DEFAULT;

                String s = getString(R.string.start_capture);
                captureButton.setText(isUploadAvailable() ? s : s + " (No upload)");
                captureButton.setTextColor(0xffffffff);
                captureButton.setBackgroundResource(R.color.colorPrimary);

                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                getWindow().getDecorView().getRootView().setBackgroundResource(R.color.colorLight);
            }
        }
    }

    private enum State {
        CAPTURE_UPLOAD, CAPTURE, UPLOAD, DEFAULT
    }

    public void refresh(MenuItem item) {
        updateState();
    }

    private void requestPermissions() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length < 2 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "You didn't give permissions!", Toast.LENGTH_LONG).show();
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_camera, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
