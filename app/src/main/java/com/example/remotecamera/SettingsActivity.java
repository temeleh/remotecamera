package com.example.remotecamera;

import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.preferences, rootKey);

            EditTextPreference delayPref = getPreferenceScreen().findPreference(getString(R.string.delay));
            EditTextPreference countPref = getPreferenceScreen().findPreference(getString(R.string.count));
            EditTextPreference backQualPref = getPreferenceScreen().findPreference(getString(R.string.backquality));
            EditTextPreference frontQualPref = getPreferenceScreen().findPreference(getString(R.string.frontquality));

            validate(delayPref, 20000, Integer.MAX_VALUE);
            validate(countPref, 0, Integer.MAX_VALUE);
            validate(backQualPref, 0, 100);
            validate(frontQualPref, 0, 100);

            EditTextPreference urlBasePref = getPreferenceScreen().findPreference(getString(R.string.url_base));
            assert urlBasePref != null;
            urlBasePref.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                @Override
                public void onBindEditText(@NonNull EditText editText) {
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
                }
            });

            ListPreference sidePref = getPreferenceScreen().findPreference(getString(R.string.cameraside));
            ListPreference backResPref = getPreferenceScreen().findPreference(getString(R.string.backres));
            ListPreference frontResPref = getPreferenceScreen().findPreference(getString(R.string.frontres));

            showValueInSummary(delayPref, false);
            showValueInSummary(countPref, false);
            showValueInSummary(backQualPref, false);
            showValueInSummary(frontQualPref, false);

            showValueInSummary(urlBasePref, true);

            showValueInSummary(sidePref, true);
            showValueInSummary(backResPref, true);
            showValueInSummary(frontResPref, true);
        }

        private void validate(final EditTextPreference editPref, final int min, final int max) {
            Objects.requireNonNull(editPref);

            editPref.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                @Override
                public void onBindEditText(@NonNull EditText editText) {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
            });

            editPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    int value;
                    try {
                        value = Integer.parseInt(newValue.toString());
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    if (value >= min && value <= max) {
                        preference.setSummary(newValue.toString());
                        return true;
                    }

                    return false;
                }
            });
        }

        private void showValueInSummary(Preference preference, boolean addListener) {
            if (preference instanceof EditTextPreference) {
                EditTextPreference p = (EditTextPreference) preference;
                p.setSummary(p.getText());
            } else if (preference instanceof ListPreference) {
                ListPreference p = (ListPreference) preference;
                p.setSummary(p.getValue());
            }

            if (addListener) {
                preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        preference.setSummary(newValue.toString());
                        return true;
                    }
                });
            }

        }
    }
}

