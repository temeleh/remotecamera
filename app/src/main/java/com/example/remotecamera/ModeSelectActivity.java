package com.example.remotecamera;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class ModeSelectActivity extends AppCompatActivity {

    private EditText passField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode_select);

        passField = findViewById(R.id.passField);
    }

    public void openCameraSetup(View view) {
        startActivityWithPassphrase(CameraActivity.class);
    }

    public void openRemoteSetup(View view) {
        startActivityWithPassphrase(RemoteActivity.class);
    }

    private void startActivityWithPassphrase(Class<?> activityClass) {
        String passphrase = passField.getText().toString();

        if (!isValidPassphrase(passphrase)) {
            Toast.makeText(this, "Invalid passphrase!", Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(this, activityClass);
        intent.putExtra(getString(R.string.passphrase), passphrase);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_modeselect, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void openSettings(MenuItem item) {
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        startActivity(settingsIntent);
    }

    private boolean isValidPassphrase(String passphrase) {
        return passphrase != null && !passphrase.isEmpty();
    }
}
