# RemoteCamera

NOT MAINTAINED!!

Android application for taking time-lapse image sequences. Server can also be selected where the latest image is synced every ~15 minutes. Compatible with my RemoteCameraServer project. Captured images are saved in a new RemoteCamera directory. I wrote this Android app (and the server) in 2019.

All activities (screens) of the application:

![img](activities.png)
